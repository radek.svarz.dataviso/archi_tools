# Archi helping scripts and tools #
Set of small scripts to help manipulate data of the .archimate file of the Archi tool ( https://www.archimatetool.com/ ) . Mostly for importing and exporting data for bulk clean up or synchronisation with other tools. Simpler than built in csv import.

Tools for:

 - updating .archimate properties of selected elements
 - updating documentation attribute of selected elements
 - exporting elements' presence in diagrams

## Requirements ##
Python 3

## Installation ##
pip install -r requirements.txt

## Execution ##
Run python scripts from command line. Help is embedded.


I propose these scripts being prototypes for java Archi implementations.

Limitations: 
 - only .archimate xml format (no packed, or DB) - tested with Archi 3 a 4
 - IDs used are the internal element IDs - we had some discussion on this topic in issue https://github.com/archimatetool/archi/issues/250s

Disclaimer - I am not liable for any changes to your data. You are advised to review the script before the usage. You are on your own.
