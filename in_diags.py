"""
Archimate export of Element presence in diagrams
================================================
:copyright: Radek Svarz / DATAVISO
MIT License - see LICENSE.txt file
"""

import argparse
import csv
import os
import lxml.etree as et
from tqdm import tqdm

cli_parser = argparse.ArgumentParser(
    description="Exports the presence of Elements in diagrams (.archimate file) into csv sheet. "
    )
cli_parser.add_argument("input_model_file", help="Input archimate model (will not be modified).")
cli_parser.add_argument("--output_sheet",
                        default="diagrams.csv",
                        help="Name of the output csv sheet (will be overwritten if exists). "
                             "If not given diagrams.csv will be used instead.")
args = cli_parser.parse_args()

namespaces = {'xsi': "http://www.w3.org/2001/XMLSchema-instance", }

# List of all diagrams as stored
# r.xpath(".//element[@xsi:type='archimate:ArchimateDiagramModel']/@name", namespaces = namespaces)

# List of all diagrams sorted
# sorted(r.xpath(".//element[@xsi:type='archimate:ArchimateDiagramModel']/@name", namespaces = namespaces))

# List of ID related diagrams
# sorted(r.xpath(".//element[@xsi:type='archimate:ArchimateDiagramModel' and .//child[@archimateElement='MUR']]/@name", namespaces = namespaces))

def domain_filter(diagram_list):
    """Filtering diagrams of domains only with the {} convention."""
    return [dom[:5] for dom in diagram_list if dom[0] == "{"]


tree = et.parse(args.input_model_file)
app_tree = tree.find(".//folder[@name='Application']")
els = app_tree.findall(".//element")

try:
    with open(args.output_sheet, "w", newline='', encoding="utf-8-sig") as csvfout:

        csvout = csv.DictWriter(csvfout, dialect="excel",
                                fieldnames=["ID", "Domains", "Info: BAC record", "Diagrams"])
        csvout.writeheader()

        for app in tqdm(els):

            diags = sorted(tree.xpath(
                ".//element[@xsi:type='archimate:ArchimateDiagramModel' and .//child[@archimateElement='%s']]/@name"
                % app.get("id"),
                namespaces=namespaces))

            if diags == []:
                domains_abbr = ""
            else:
                domains_abbr = ", ".join(domain_filter(diags))

            diagrams = ", ".join(diags)

            row = {"ID": app.get("id"),
                   "Domains": domains_abbr,
                   "Info: BAC record": app.get("name"),
                   "Diagrams": diagrams,
                   }
            csvout.writerow(row)

except IOError as e:
    print('Operation failed: %s' % e.strerror)
