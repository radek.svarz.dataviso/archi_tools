"""
Archimate file properties update
====================================
:copyright: Radek Svarz / DATAVISO
MIT License - see LICENSE.txt file
"""

import argparse
import csv
import os
import lxml.etree as et
from tqdm import tqdm

cli_parser = argparse.ArgumentParser(
    description="Updates the element properties of the Archimate model (.archimate file) based on the input csv file. "
                "CSV file structure: ID, Property value. "
                "Input header names are ignored. Property is selected either by --property flag or "
                "by input csv filename without extension. "
                "Input model is not overwritten, the new model is created instead with additional protocol csv sheet."
    )
cli_parser.add_argument("input_model_file", help="Input archimate model (will not be modified).")
cli_parser.add_argument("input_csv_file", help="Input property csv file (will not be modified).")
cli_parser.add_argument("--property",
                        help="Property name to be updated in the model. Transformed to uppercase. "
                             "If not given the input_csv_file filename is used instead.")
cli_parser.add_argument("--output_model_file",
                        help="Name of the output archimate model (will be overwritten if exists). "
                             "If not given the input filename is used with added _out.archimate.")
cli_parser.add_argument("--protocol_sheet",
                        help="Name of the output protocol csv file (will be overwritten if exists). "
                             "If not given the output model filename is used with added _protocol.csv")

args = cli_parser.parse_args()

if args.property is None:
    property_kind = os.path.splitext(os.path.basename(args.input_csv_file))[0]
else:
    property_kind = args.property
property_kind = property_kind.upper()

if args.output_model_file is None:
    args.output_model_file = os.path.splitext(args.input_model_file)[0] + "_out.archimate"

if args.protocol_sheet is None:
    args.protocol_sheet = os.path.splitext(args.output_model_file)[0] + "_" + property_kind + "_protocol.csv"

tree = et.parse(args.input_model_file)

try:
    with open(args.input_csv_file, newline='', encoding="utf-8-sig") as csvfin, \
            open(args.protocol_sheet, "w", newline='', encoding="utf-8-sig") as csvfout:

        csvincounter = csv.DictReader(csvfin, dialect="excel")
        totalrecs = 0
        for row in csvincounter:
            totalrecs += 1
        csvfin.seek(0)

        csvin = csv.DictReader(csvfin, dialect="excel")
        csvout = csv.DictWriter(csvfout, dialect="excel",
                                fieldnames=["ID", "Value", "Result", "Old value"])
        csvout.writeheader()
        # Ignore the input header names and set to default
        csvin.fieldnames[0] = "ID"
        csvin.fieldnames[1] = "Value"
        for row in tqdm(csvin, total=totalrecs):
            prop = tree.find((".//element[@id='%s']/property[@key='%s']" % (row["ID"], property_kind)))
            if prop is None:
                elem = tree.find((".//element[@id='%s']" % (row["ID"])))
                if elem is None:
                    # ID does not exist in the model
                    row["Result"] = "ID Not Found"
                else:
                    # Property for this ID does not exist, creating the new with the value
                    new_prop = et.Element("property", key=property_kind, value=row["Value"])
                    elem.append(new_prop)
                    row["Result"] = "Created new %s property" % property_kind
            else:
                # Property for this ID already exists, replacing the value
                row["Result"] = "Replaced"
                row["Old value"] = prop.get("value")
                prop.set("value", row["Value"])
            csvout.writerow(row)
except IOError as e:
    print('Operation failed: %s' % e.strerror)

tree.write(args.output_model_file, xml_declaration=True, encoding="UTF-8")
