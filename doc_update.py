"""
Archimate file - documentation update
====================================
:copyright: Radek Svarz / DATAVISO
MIT License - see LICENSE.txt file
"""

import argparse
import csv
import os
import lxml.etree as et
from tqdm import tqdm

cli_parser = argparse.ArgumentParser(
    description="Updates documentation content of the element within Archimate model (.archimate file) "
                "based on the input csv file. "
                "CSV file structure: ID, Documentation multiline text. "
                "Input header names are ignored."
                "Input model is not overwritten, the new model is created instead with additional protocol csv sheet."
    )
cli_parser.add_argument("input_model_file", help="Input archimate model (will not be modified).")
cli_parser.add_argument("input_csv_file", help="Input documentation csv file (will not be modified).")
cli_parser.add_argument("--output_model_file",
                        help="Name of the output archimate model (will be overwritten if exists). "
                             "If not given the input filename is used with added _out.archimate.")
cli_parser.add_argument("--protocol_sheet",
                        help="Name of the output protocol csv file (will be overwritten if exists). "
                             "If not given the output model filename is used with added _protocol.csv")

args = cli_parser.parse_args()

if args.output_model_file is None:
    args.output_model_file = os.path.splitext(args.input_model_file)[0] + "_out.archimate"

if args.protocol_sheet is None:
    args.protocol_sheet = os.path.splitext(args.input_model_file)[0] + "_doc_protocol.csv"

tree = et.parse(args.input_model_file)

try:
    with open(args.input_csv_file, newline='', encoding="utf-8-sig") as csvfin, \
            open(args.protocol_sheet, "w", newline='', encoding="utf-8-sig") as csvfout:

        csvincounter = csv.DictReader(csvfin, dialect="excel")
        totalrecs = 0
        for row in csvincounter:
            totalrecs += 1
        csvfin.seek(0)

        csvin = csv.DictReader(csvfin, dialect="excel")
        csvout = csv.DictWriter(csvfout, dialect="excel",
                                fieldnames=["ID", "Description", "Result", "Old description"])
        csvout.writeheader()

        # Ignore the input header names and set to default
        csvin.fieldnames[0] = "ID"
        csvin.fieldnames[1] = "Description"
        for row in tqdm(csvin, total=totalrecs):
            doc = tree.find((".//element[@id='%s']/documentation" % (row["ID"], )))
            if doc is None:
                elem = tree.find((".//element[@id='%s']" % (row["ID"])))
                if elem is None:
                    # ID does not exist in the model
                    row["Result"] = "ID Not Found"
                else:
                    # Doc for this ID does not exist, creating the new with the value
                    new_doc = et.Element("documentation")
                    new_doc.text = row["Description"]
                    elem.append(new_doc)
                    row["Result"] = "Created new Documentation string"
            else:
                # Doc for this ID already exists, replacing the value
                row["Result"] = "Replaced"
                row["Old description"] = doc.text
                doc.text = row["Description"]
            csvout.writerow(row)
except IOError as e:
    print('Operation failed: %s' % e.strerror)

tree.write(args.output_model_file, xml_declaration=True, encoding="UTF-8")
